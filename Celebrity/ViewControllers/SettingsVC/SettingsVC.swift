//
//  SettingsVC.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 22/07/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import SwiftIconFont

class SettingsVC: UIViewController{
    
    var teamCount: Int = 2;
    var roundCount: Int = 2;
    var timeInSec: Int = 60;
    var allowDuplic: Bool = false;
    
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblRound: UILabel!
    @IBOutlet weak var lblAllow: UILabel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //
    override func viewDidLoad() {
        //
        if let data = SettingsDataHelper.returnSettings() as? Settings{
            self.teamCount = Int(data.teamCount);
            self.timeInSec = Int(data.timeInSec);
            self.roundCount = Int(data.roundCount);
            self.allowDuplic = Bool(data.allowDuplicate);
            
            self.lblTeam.text = "No. Of Teams - \(self.teamCount)";
            self.lblTime.text = "Turn Time - \(UtilityHelper.secIntoFormat(self.timeInSec))";
            self.lblRound.text = "No. Of Rounds - \(self.roundCount)";
            self.lblAllow.text = "Duplicates - \(self.allowDuplic ? "Yes" : "No")"
            
        }
        
        
        
    }
    
    @IBAction func popback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func plusTeamCount(_ sender: UIButton) {
        //
        var c = self.teamCount;
        c = c + 1;
        if(c > 4){
            c = 2
        }
        self.teamCount = c;
        self.lblTeam.text = "No. Of Teams - \(c)";
        
        setSettings()
        
    }
    
    @IBAction func minusTeamCount(_ sender: UIButton) {
        //
        var c = self.teamCount;
        c = c - 1;
        if(c <= 1){
            c = 4
        }
        self.teamCount = c;
        self.lblTeam.text = "No. Of Teams - \(c)";
        
        setSettings()
        
    }
    
    @IBAction func plusRoundCount(_ sender: UIButton) {
        //
        var c = self.roundCount;
        c = c + 1;
        if(c > 4){
            c = 2
        }
        self.roundCount = c;
        self.lblRound.text = "No. Of Rounds - \(c)";
        
        setSettings()
        
    }
    
    @IBAction func minusRoundCount(_ sender: UIButton) {
        //
        var c = self.roundCount;
        c = c - 1;
        if(c <= 1){
            c = 4
        }
        self.roundCount = c;
        self.lblRound.text = "No. Of Rounds - \(c)";
        
        setSettings()
        
    }
    
    @IBAction func plusTimeCount(_ sender: UIButton) {
        //
        var c = self.timeInSec;
        c = c + 20;
        if(c > 120){
            c = 60
        }
        self.timeInSec = c;
        self.lblTime.text = "Turn Time - \(UtilityHelper.secIntoFormat(self.timeInSec))";
        
        setSettings()
        
    }
    
    @IBAction func minusTimeCount(_ sender: UIButton) {
        //
        var c = self.timeInSec;
        c = c - 20;
        if(c <= 60){
            c = 120
        }
        self.timeInSec = c;
        self.lblTime.text = "Turn Time - \(UtilityHelper.secIntoFormat(self.timeInSec))";
        
        setSettings()
        
    }
    
    @IBAction func changeDuplicateCount(_ sender: UIButton) {
        //
        var c = self.allowDuplic;
        c = !c;
        self.allowDuplic = c
        
        self.lblAllow.text = "Duplicates - \(self.allowDuplic ? "Yes" : "No")"
        
        setSettings()
        
    }
    
    private func setSettings(){
        SettingsDataHelper.updateSettings(self.teamCount, self.roundCount, self.timeInSec, self.allowDuplic) { (obj) in
            print(obj);
        }
    }
    
    
    
        
    
    
    
}

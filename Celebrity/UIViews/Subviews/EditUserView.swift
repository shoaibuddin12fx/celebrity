//
//  AddUserView.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 22/07/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import DropDown


@objc protocol EditUserViewDelegate {
    func editUserFromList(view : EditUserView, obj: NSManagedObject, name: String);
    func removeEditVIew(view : EditUserView);
}

class EditUserView: UIView, UITextFieldDelegate{
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var txtName: UITextField!
    
    weak var delegate:EditUserViewDelegate?;
    var user: NSManagedObject!;
    
    
    
    override func awakeFromNib() {
        //
        self.layoutSubviews();
        txtName.delegate = self;
        txtName.becomeFirstResponder();
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func setData(user: NSManagedObject){
        self.user = user
        let u = self.user as? Users
        self.txtName.text = u?.name
    }
    
    
    
    
    
    
    
    
    @IBAction func cancelView(_ sender: UIButton) {
        self.delegate?.removeEditVIew(view: self);
    }
    
    @IBAction func addView(_ sender: UIButton) {
        guard let t = self.txtName.text, t != "" else { return };
        self.delegate?.editUserFromList(view: self, obj: self.user, name: t)
    }
    

    
    
    
}

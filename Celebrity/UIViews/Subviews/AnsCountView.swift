//
//  AnsCountView.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 01/08/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import SwiftIconFont

@objc protocol AnsCountViewDelegate {
    func SwitchView(view : AnsCountView, remainCount: Int);
}

class AnsCountView: UIView, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var btnNextPlayer: UIButton!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var imgWrongicon: UIImageView!
    @IBOutlet weak var imgWriteicon: UIImageView!
    
    @IBOutlet weak var wrongCollection: UICollectionView!
    @IBOutlet weak var writeCollection: UICollectionView!
    
    var celebArrayRite: [Celeb] = [Celeb]();
    var celebArrayWrong: [Celeb] = [Celeb]();
    var iUser : Users!;
    weak var delegate:AnsCountViewDelegate?;
    var remainCount = -1;
    var nuser: Users!;
    
    
    
    override func awakeFromNib() {
        //
//        imgWriteicon.backgroundColor = CSS.colorWithHexString(Colors.green);
//        imgWriteicon.roundImageBorder(color: Colors.white, width: 5);
//        imgWrongicon.backgroundColor = CSS.colorWithHexString(Colors.red);
//        imgWrongicon.roundImageBorder(color: Colors.white, width: 5);
//        CSS.setFontImageVisualsMaterial(imgWrongicon, name: "close", color: Colors.white);
//        CSS.setFontImageVisualsMaterial(imgWriteicon, name: "done", color: Colors.white);
        
        wrongCollection.delegate = self;
        writeCollection.delegate = self;
        wrongCollection.dataSource = self;
        writeCollection.dataSource = self;
        
        wrongCollection.register(UINib(nibName: "CelebCVC", bundle: nil), forCellWithReuseIdentifier: "CelebCVC");
        wrongCollection.delegate = self;
        wrongCollection.dataSource = self;
        wrongCollection.allowsMultipleSelection = false;
        wrongCollection.dragInteractionEnabled = true;
        if #available(iOS 11.0, *) {
            wrongCollection?.contentInsetAdjustmentBehavior = .always
        }
        
        writeCollection.register(UINib(nibName: "CelebCVC", bundle: nil), forCellWithReuseIdentifier: "CelebCVC");
        writeCollection.delegate = self;
        writeCollection.dataSource = self;
        writeCollection.allowsMultipleSelection = false;
        writeCollection.dragInteractionEnabled = true;
        if #available(iOS 11.0, *) {
            writeCollection?.contentInsetAdjustmentBehavior = .always
        }
        
        
        //
        // add a longPress gesture to writeCollectionView
        let lpgr1 = UILongPressGestureRecognizer(target: self, action: #selector(handleWriteCollectionViewLongPress(gestureReconizer:)))
        lpgr1.minimumPressDuration = 0.5
        lpgr1.delaysTouchesBegan = true
        lpgr1.delegate = self
        self.writeCollection.addGestureRecognizer(lpgr1)
        
        // add a longPress gesture to wrongCollectionView
        let lpgr2 = UILongPressGestureRecognizer(target: self, action: #selector(handleWrongCollectionViewLongPress(gestureReconizer:)))
        lpgr2.minimumPressDuration = 0.5
        lpgr2.delaysTouchesBegan = true
        lpgr2.delegate = self
        self.wrongCollection.addGestureRecognizer(lpgr2)
        
        
        
        
        
    }
    
    var writeCindex = -1;
    var wrongCindex = -1;
    var vwCellSwipe: UIImageView = UIImageView();
    
    @objc func handleWriteCollectionViewLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        
        switch(gestureReconizer.state)
        {
            
        case UIGestureRecognizerState.began:
            //
            guard let selectedIndexPath = self.writeCollection.indexPathForItem(at: gestureReconizer.location(in: self.writeCollection))
                else
            {
                break
            }
            
            writeCindex = selectedIndexPath.row;
            
            let cell = self.writeCollection.cellForItem(at: selectedIndexPath) as! CelebCVC
            
            self.vwCellSwipe.frame = CGRect(x: cell.frame.origin.x, y: (self.writeCollection.superview?.frame.origin.y)! + self.writeCollection.frame.origin.y +  cell.frame.origin.y, width: cell.frame.size.width, height: cell.frame.size.height);
            print(cell.frame);
            self.vwCellSwipe.image = cell.snapshot();
            self.vwCellSwipe.backgroundColor = UIColor(white: 1.0, alpha: 0.5);
            //self.imgViewCellSwipe.image = cell.imgIcons.image
            
            let Window: UIWindow = UIApplication.shared.keyWindow!;
            Window.addSubview(self.vwCellSwipe)
            //self.bringSubview(toFront: self.vwCellSwipe)
            //self.vwCellSwipe.isHidden = false;
            
        case UIGestureRecognizerState.changed:
            self.vwCellSwipe.center = gestureReconizer.location(in: self)
            
        case UIGestureRecognizerState.ended:
            
            guard let destinationIndexPath = self.wrongCollection
                else
            {
                UIView.animate(withDuration: 0.1, animations: {
                    
                }, completion: { (true) in
                    
                    self.bringSubview(toFront: self.writeCollection)
                    self.vwCellSwipe.removeFromSuperview();
                })
                
                break
            }
            
            //Replace Celeb
            let ind = IndexPath(row: writeCindex, section: 0);
            collectionView(writeCollection, didSelectItemAt: ind);
            
            self.bringSubview(toFront: writeCollection)
            self.vwCellSwipe.removeFromSuperview();
            
            
            
        default:
            if #available(iOS 9.0, *) {
                self.writeCollection.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @objc func handleWrongCollectionViewLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        
        switch(gestureReconizer.state)
        {
            
        case UIGestureRecognizerState.began:
            //
            guard let selectedIndexPath = self.wrongCollection.indexPathForItem(at: gestureReconizer.location(in: self.wrongCollection))
                else
            {
                break
            }
            
            wrongCindex = selectedIndexPath.row;
            
            let cell = self.wrongCollection.cellForItem(at: selectedIndexPath) as! CelebCVC
            
            self.vwCellSwipe.frame = CGRect(x: cell.frame.origin.x, y: (self.writeCollection.superview?.frame.origin.y)! + self.writeCollection.frame.origin.y +  cell.frame.origin.y, width: cell.frame.size.width, height: cell.frame.size.height);
            print(cell.frame);
            self.vwCellSwipe.image = cell.snapshot();
            self.vwCellSwipe.backgroundColor = UIColor(white: 1.0, alpha: 0.5);
            //self.imgViewCellSwipe.image = cell.imgIcons.image
            
            let Window: UIWindow = UIApplication.shared.keyWindow!;
            Window.addSubview(self.vwCellSwipe)
            //self.bringSubview(toFront: self.vwCellSwipe)
            //self.vwCellSwipe.isHidden = false;
            
        case UIGestureRecognizerState.changed:
            self.vwCellSwipe.center = gestureReconizer.location(in: self)
            
        case UIGestureRecognizerState.ended:
            
            guard let destinationIndexPath = self.writeCollection
                else
            {
                UIView.animate(withDuration: 0.1, animations: {
                    
                }, completion: { (true) in
                    
                    self.bringSubview(toFront: self.wrongCollection)
                    self.vwCellSwipe.removeFromSuperview();
                })
                
                break
            }
            
            //Replace Celeb
            let ind = IndexPath(row: wrongCindex, section: 0);
            collectionView(wrongCollection, didSelectItemAt: ind);
            
            self.bringSubview(toFront: wrongCollection)
            self.vwCellSwipe.removeFromSuperview();
            
            
            
        default:
            if #available(iOS 9.0, *) {
                self.wrongCollection.cancelInteractiveMovement()
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    
    
    func setData( user: Users, write : [Celeb] , wrong : [Celeb], nextuser: Users!, remains: [Celeb]){
        celebArrayRite = write;
        celebArrayWrong = wrong;
        self.iUser = user;
        self.lblPlayerName.text = "\(user.name!) score"
        
        
        
        remainCount = remains.count;
        nuser = nextuser;
        
        checkRemains()
        
        
        self.wrongCollection.reloadData();
        self.writeCollection.reloadData();
    }
    
    func checkRemains(){
        
        var ltitle = "";
        
        if(remainCount == 0){
            ltitle = "Turns Completed";
        }else{
            ltitle =  "\(nuser.name!)'s turn"
        }
        
        self.btnNextPlayer.setTitle(ltitle, for: .normal)
    }
    
    @IBAction func nextRound(_ sender: UIButton) {
        
        var intt = Int(self.iUser.counter);
        intt = intt + celebArrayRite.count;
        UserDataHelper.updateUser(iuser: self.iUser, count: intt);
        print(intt);
        
        
        delegate?.SwitchView(view: self, remainCount: remainCount);
        
    }
    
    
    
}

extension AnsCountView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        collectionView.layoutIfNeeded();
        let width = collectionView.frame.width;
//        let height = collectionView.frame.height/5;
        return CGSize(width: width, height: 25.5);
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Dequeue a GridViewCell.
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CelebCVC.self), for: indexPath) as? CelebCVC
            else { fatalError("unexpected cell in collection view") }
        
        if(collectionView == self.writeCollection){
            cell.setData(celebArrayRite[indexPath.row]);
        }else{
            cell.setData(celebArrayWrong[indexPath.row]);
        }
        
        
        return cell;
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        //
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //
        if(collectionView == self.writeCollection){
            return celebArrayRite.count;
        }else{
            return celebArrayWrong.count;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //
        return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        let i = indexPath;
        if(collectionView == self.writeCollection){
            
            celebArrayWrong.append(celebArrayRite[i.row]);
            celebArrayRite.remove(at: i.row);
            remainCount = remainCount + 1;
            
        }else{
            
            celebArrayRite.append(celebArrayWrong[i.row]);
            celebArrayWrong.remove(at: i.row);
            
            remainCount = remainCount - 1;
            
        }
        
        checkRemains();
        
        
        
        self.writeCollection.reloadData();
        self.wrongCollection.reloadData();
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //
        
        
        
    }
    
    
    
    
}



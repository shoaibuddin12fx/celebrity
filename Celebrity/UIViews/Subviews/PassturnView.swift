//
//  PassturnView.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 23/07/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@objc protocol PassturnViewDelegate {
    func closePassturnView(view : PassturnView);
    
}

class PassturnView: UIView{
    
    var pname = "";
    var obj: NSManagedObject!;
    @IBOutlet weak var txtName: UILabel!
    weak var delegate:PassturnViewDelegate?;
    
    override func awakeFromNib() {
        //
        
        
    }
    
    func setData(name: String){
        self.txtName.text = "Pass device to \(name)";
    }
    
    
    
    @IBAction func cancelView(_ sender: UIButton) {
        self.delegate?.closePassturnView(view: self);
        self.removeFromSuperview();
    }
    
    
    
    
}


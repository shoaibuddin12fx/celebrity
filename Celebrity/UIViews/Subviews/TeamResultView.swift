//
//  TeamResultView.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 02/08/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit

@objc protocol TeamResultViewDelegate {
    func SwitchRound(view : TeamResultView);
}

class TeamResultView: UIView {
    //
    
    @IBOutlet weak var lblRcount: UILabel!
    @IBOutlet weak var lblMoreRound: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    weak var delegate:TeamResultViewDelegate?;
    var UserArray: [Users]!;
    var teamCount = 4;
    var roundCount = 2;
    var teams: [[Users]] = [[Users]]();
    
    override func awakeFromNib() {
        //
        collectionView.register(UINib(nibName: "ResultCVC", bundle: nil), forCellWithReuseIdentifier: "ResultCVC");
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.allowsMultipleSelection = false;
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .always
        }
        
        
        
    }
    
    func setData(_ rcount: Int){
        
        var st = SettingsDataHelper.returnSettings() as! Settings;
        self.teamCount = Int(st.teamCount)
        self.roundCount = Int(st.roundCount);
        self.lblRcount.text = "Round \(rcount) Finished";
        self.lblMoreRound.text = "1 More Round !";
        self.btnNext.setTitle("Next Round !", for: .normal);
        
        if(rcount == self.roundCount){
            
            self.btnNext.setTitle("Close Game", for: .normal);
        }
        
        
        UserDataHelper.getUsersTeamwise { (teams) in
            self.teams = teams;
            
            // who wins ?
            if(rcount == self.roundCount){
                
                
                var winner = 1;
                
                var outer_c = 0;
                var inner_c = 0;
                
                for team in self.teams{
                    
                    var n_c = 0
                    for t in team{
                        n_c = n_c + Int(t.counter);
                    }
                    
                    if(n_c > outer_c){
                        outer_c = n_c;
                        winner = Int(team[0].teamId);
                    }
                    
                }
                
                self.lblMoreRound.text = "Team \(winner) Wins !";
            }
            
            
            
            
            
            
            self.collectionView.reloadData();
        };
        
//        self.UserArray = users;
        
    }
    
    @IBAction func playAgain(_ sender: UIButton) {
        delegate?.SwitchRound(view: self);
    }
    
}

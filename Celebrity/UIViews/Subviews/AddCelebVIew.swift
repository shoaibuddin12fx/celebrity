//
//  AddCelebVIew.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 23/07/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@objc protocol AddCelebViewDelegate {
    func refreshCelebList(view : AddCelebView);
    
}

class AddCelebView: UIView, UITextFieldDelegate{
    
    var pname = "";
    var obj: NSManagedObject!;
    @IBOutlet weak var txtName: UITextField!
    weak var delegate:AddCelebViewDelegate?;
    
    override func awakeFromNib() {
        //
        txtName.delegate = self;
        txtName.becomeFirstResponder();
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func setData(name: String){
        self.pname = name;
    }
    
    func setData(obj: NSManagedObject){
        self.obj = obj;
    }
    
    @IBAction func cancelView(_ sender: UIButton) {
        self.delegate?.refreshCelebList(view: self);
        self.removeFromSuperview();
    }
    
    @IBAction func addView(_ sender: UIButton) {
        
        let t = self.txtName.text!;
        if(t == "" || t == nil){ return }
        
        CelebDataHelper.createCeleb(t, userObj: obj) { (obj) in
            self.delegate?.refreshCelebList(view: self);
            self.removeFromSuperview();
        }
        
    }
    
    
    
}


//
//  ExtensionHelper.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 01/08/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit


extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            // Change `Int` in the next line to `IndexDistance` in < Swift 4.1
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

extension UIView{
    
    func snapshot(of rect: CGRect? = nil) -> UIImage? {
        // snapshot entire view
        
        if(rect == nil){
            UIGraphicsBeginImageContextWithOptions(CGSize(width: round(self.bounds.size.width), height: round(self.bounds.size.height)), false, 0.0)
            let contextx: CGContext? = UIGraphicsGetCurrentContext()
            self.layer.render(in: contextx!)
            let capturedScreen: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return capturedScreen!
        }else{
            
            UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0);
            
            
            //UIGraphicsBeginImageContext(self.frame.size)
            drawHierarchy(in: bounds, afterScreenUpdates: true)
            let wholeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // if no `rect` provided, return image of whole view
            
            guard let image = wholeImage, let rect = rect else { return wholeImage }
            
            // otherwise, grab specified `rect` of image
            
            let scale = image.scale
            let scaledRect = CGRect(x: rect.origin.x * scale, y: rect.origin.y * scale, width: rect.size.width * scale, height: rect.size.height * scale)
            guard let cgImage = image.cgImage?.cropping(to: scaledRect) else { return nil }
            return UIImage(cgImage: cgImage, scale: scale, orientation: .up)
            
        }
        
    }
    
    
    func roundImageBorder(color : String, width: CGFloat){
        let v = self;
        v.layoutIfNeeded();
        v.layer.cornerRadius = v.frame.size.width/2;
        v.clipsToBounds = true;
        v.layer.borderColor = CSS.colorWithHexString(color).cgColor;
        v.layer.borderWidth = width;
    }
    
    func setBorder(color: String, radius: Int, width: Int){
        self.layoutIfNeeded();
        self.layer.cornerRadius = CGFloat(radius);
        self.clipsToBounds = true;
        self.layer.borderColor = CSS.colorWithHexString(color).cgColor;
        self.layer.borderWidth = CGFloat(width);
    }
    
    
    
}

extension Array {
    mutating func rearrange(from: Int, to: Int) {
        insert(remove(at: from), at: to)
    }
}

